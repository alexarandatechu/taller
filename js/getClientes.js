var respuestaClientes;

function getClientes() {
  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      console.log(this.responseText);
      respuestaClientes = this.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true)
  request.send();
}

function procesarClientes() {
  var JSONClientes = JSON.parse(respuestaClientes);
  var table = document.getElementById('bodyClientes');
  for (var i = 0; i < JSONClientes.value.length; i++) {
    var nuevaFila = document.createElement("tr");
    var cliente = JSONClientes.value[i];
    addColumna(nuevaFila, cliente.ContactName);
    addColumna(nuevaFila, cliente.CompanyName);
    addColumna(nuevaFila, cliente.Phone);
    addColumna(nuevaFila, cliente.City);
    addColumnaImagen(nuevaFila, cliente.Country);
    table.appendChild(nuevaFila);
  }
}

function addColumna(fila, valor) {
  var columna = document.createElement("td");
  columna.innerText = valor;
  fila.appendChild(columna);
}

function addColumnaImagen(fila, valor) {
  var columna = document.createElement("td");
  var img = document.createElement("img");
  img.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-" + valor + ".png";
  img.classList.add("flag");
  columna.appendChild(img);

  fila.appendChild(columna);

}
